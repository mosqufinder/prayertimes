package com.mosquefinder.arnal.prayertimesapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.exoplayer.ExoPlayer;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.mosquefinder.arnal.prayertimesapp.data.HijriTime;
import com.mosquefinder.arnal.prayertimesapp.data.MiladiTime;
import com.mosquefinder.arnal.prayertimesapp.data.PrayTimes;
import com.mosquefinder.arnal.prayertimesapp.data.TimesPreferences;
import com.mosquefinder.arnal.prayertimesapp.service.AthanService;

import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormatter;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import timber.log.Timber;

import static com.mosquefinder.arnal.prayertimesapp.data.PrayTimes.getMinutes;

public class MainActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    private final String TAG = MainActivity.class.getSimpleName();
    private static final int PLACE_PICKER_REQUEST = 1;
    double latitude, longitude ;
    TextView city, date_geo, date_hijr, time,fajr, sunrise, zuhr, asr, magreeb, isha, next_prayer_name, method_label_textView;
    LinearLayout fajr_back, sunrise_back, zuhr_back, asr_back, magreeb_back, isha_back;
    ImageButton place;
    HijriTime hijritime;
    Context context;

    private static int[] allPrayrTimesInMinutes;
    public static int nextPrayerTimeInMinutes;//next prayer time in minutes
    public static int actualPrayerCode;
    public static int missing_hours_to_nextPrayer;
    public static int missing_minutes_to_nextPrayer;
    public static int missing_seconds_to_nextPrayer;
    public static long nextPrayerinMilSeconds;
    public static int currentTimeMinute;

    private ExoPlayer exoPlayer;
    private MediaPlayer player;
    private AudioManager mAudioManager;
    private MediaPlayer.OnCompletionListener mCompletionListener = new MediaPlayer.OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            // Now that the sound file has finished playing, release the media player resources.
            releaseMediaPlayer();
        }
    };
    private AudioManager.OnAudioFocusChangeListener mOnAudioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
        @Override
        public void onAudioFocusChange(int focusChange) {
            if (focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT ||
                    focusChange == AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK) {
                // The AUDIOFOCUS_LOSS_TRANSIENT case means that we've lost audio focus for a
                // short amount of time. The AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK case means that
                // our app is allowed to continue playing sound but at a lower volume. We'll treat
                // both cases the same way because our app is playing short sound files.
                // Pause playback and reset player to the start of the file. That way, we can
                // play the word from the beginning when we resume playback.
                player.pause();
                player.seekTo(0);
            } else if (focusChange == AudioManager.AUDIOFOCUS_GAIN) {
                // The AUDIOFOCUS_GAIN case means we have regained focus and can resume playback.
                player.start();
            } else if (focusChange == AudioManager.AUDIOFOCUS_LOSS) {
                // The AUDIOFOCUS_LOSS case means we've lost audio focus and
                // Stop playback and clean up resources
                releaseMediaPlayer();
            }
        }
    };

    private int totalMinutes;//next prayer time in minutes

    private int hour;// hour
    private int minutes;// minutes
    private int second;// seconds

    private boolean isTimePlus1 = true;

    private TimerTask timerTask;
    private Timer timer;
    private int counter = 0;
    public NumberFormat formatter = new DecimalFormat("00");
    private enum TimerState {
        STOPPED,
        RUNNING
    }
    private TimerState mState;
    public static CountDownTimer countDownTimer;

    static int FAJR = 0;
    static int SUNRISE = 1;
    static int ZUHR = 2;
    static int ASR = 3;
    static int MAGREEB = 4;
    static int ISHA = 6;
    public static Context contextOfApplication;
    public static Context getContextOfApplication(){
        return contextOfApplication;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        city = (TextView)findViewById(R.id.location_city);
        date_geo = (TextView)findViewById(R.id.miladi_time);
        date_hijr = (TextView)findViewById(R.id.hijri_time);
        time = (TextView)findViewById(R.id.missing_time);
        next_prayer_name = (TextView)findViewById(R.id.next_prayer_name);
        fajr = (TextView) findViewById(R.id.fajr_time);
        sunrise = (TextView) findViewById(R.id.sunrise_time);
        zuhr = (TextView) findViewById(R.id.zuhr_time);
        asr = (TextView) findViewById(R.id.asr_time);
        magreeb = (TextView)findViewById(R.id.magreeb_time);
        isha = (TextView) findViewById(R.id.isha_time);
        method_label_textView = (TextView) findViewById(R.id.method_name);
        place = (ImageButton) findViewById(R.id.place_image);
        contextOfApplication = getApplicationContext();
        mState = TimerState.STOPPED;
        mAudioManager = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
        fajr_back = (LinearLayout)findViewById(R.id.fajr_background);
        sunrise_back = (LinearLayout)findViewById(R.id.sunrise_background);
        zuhr_back = (LinearLayout)findViewById(R.id.zuhr_background);
        asr_back = (LinearLayout)findViewById(R.id.asr_background);
        magreeb_back = (LinearLayout)findViewById(R.id.magreeb_background);
        isha_back = (LinearLayout)findViewById(R.id.isha_background);
        Timber.plant(new Timber.DebugTree());


        double coord[] =  TimesPreferences.getLocationCoordinates(this);

        latitude = coord[0];
        longitude = coord[1];
        refreshUI(latitude, longitude);








        // Register the listener
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    private void setPrayerName() {
        switch (actualPrayerCode){
            case 1020:
                next_prayer_name.setText(R.string.sunrise);
               break;
            case 1021:
                next_prayer_name.setText(R.string.zuhr);
                break;
            case 1022:
                next_prayer_name.setText(R.string.asr);
                break;
            case 1023:
                next_prayer_name.setText(R.string.magreeb);
                break;
            case 1024:
                next_prayer_name.setText(R.string.isha);
                 break;
            case 1025:
                next_prayer_name.setText(R.string.fajr);
                 break;
            default:
                break;
        }

    }

    private void startTimer( ){

       /* if(nextPrayerTimeInMinutes == currentTimeMinute){
            playAhan();
        }else {*/
        getNextPrayer();
            nextPrayerTimeInMinutes = nextPrayerTimeInMinutes - currentTimeMinute;

            nextPrayerinMilSeconds = TimeUnit.MINUTES.toMillis(nextPrayerTimeInMinutes);

            countDownTimer = new CountDownTimer(nextPrayerinMilSeconds + 1, 1000) {

                public void onTick(long millisUntilFinished) {


                    //time.setText("" + millisUntilFinished / 1000);

                    int input = (int) (millisUntilFinished / 1000);
                    int numberOfHours;
                    int numberOfMinutes;
                    int numberOfSeconds;


                    numberOfHours = (input % 86400) / 3600;
                    numberOfMinutes = ((input % 86400) % 3600) / 60;
                    numberOfSeconds = ((input % 86400) % 3600) % 60;
                    Timber.d(formatter.format(numberOfHours) + ":" + formatter.format(numberOfMinutes) + ":" + formatter.format(numberOfSeconds));
                    //time.setText(formatter.format(missing_hours_to_nextPrayer)+":"+formatter.format(missing_minutes_to_nextPrayer)+":"+formatter.format(missing_seconds_to_nextPrayer));
                    time.setText(formatter.format(numberOfHours) + ":" + formatter.format(numberOfMinutes) + ":" + formatter.format(numberOfSeconds));
                    setPrayerName();
                }

                public void onFinish() {

                    //  mTextView.setText("Time Up!");
                    time.setText("Now");
                /*Intent intent = new Intent(MainActivity.this, AthanActivity.class);
                startActivity(intent);*/

                    onTimerFinish();
                }

            };

            countDownTimer.start();

    }

    private void changeBackground() {
        switch (actualPrayerCode) {
            case 1021:
                fajr_back.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                sunrise_back.setBackgroundColor(getResources().getColor(R.color.colorBlueLight)); break;
            case 1022:
                sunrise_back.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                zuhr_back.setBackgroundColor(getResources().getColor(R.color.colorBlueLight)); break;
            case 1023:
                zuhr_back.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                asr_back.setBackgroundColor(getResources().getColor(R.color.colorBlueLight)); break;
            case 1024:
                asr_back.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                magreeb_back.setBackgroundColor(getResources().getColor(R.color.colorBlueLight)); break;
            case 1025:
                magreeb_back.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                isha_back.setBackgroundColor(getResources().getColor(R.color.colorBlueLight)); break;
            case 1020:
                isha_back.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                fajr_back.setBackgroundColor(getResources().getColor(R.color.colorBlueLight)); break;
            default:  break;
        }
    }

    private void playAhan() {

        releaseMediaPlayer();

        int result = mAudioManager.requestAudioFocus(mOnAudioFocusChangeListener,
                AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            // We have audio focus now.
            // Create and setup the {@link MediaPlayer} for the audio resource associated
            // with the current word
            //Log.d("AudioSourceInsideFromDB", Integer.toString(data.getInt(7)));
           // Log.d("AudioSourceInside", Integer.toString(R.raw.dua_first));


                player = MediaPlayer.create(MainActivity.this, R.raw.mohammad_khalil_raml);
                player.setLooping(false);
                player.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        player.start();
                    }
                });
                player.setOnCompletionListener(mCompletionListener);

            // Start the audio file


            // Setup a listener on the media player, so that we can stop and release the
            // media player once the sound has finished playing.

           // exoPlayer = ExoPlayer.Factory.newInstance(RENDERER_COUNT, minBufferMs, minRebufferMs);


        }






/*
        try {
            if (mediaPlayer.isPlaying()) {
                mediaPlayer.stop();
                mediaPlayer.release();
                mediaPlayer = new MediaPlayer();
            }

            String athanName = UserConfig.getSingleton().getAthan();
            String athan = "ali_ben_ahmed_mala.mp3";//athan path

            if(athanName.equalsIgnoreCase("ali_ben_ahmed_mala")){
                athan = "ali_ben_ahmed_mala.mp3";
            }else{
                if(athanName.equalsIgnoreCase("abd_el_basset_abd_essamad")){
                    athan = "abd_el_basset_abd_essamad.mp3";
                }else{
                    if(athanName.equalsIgnoreCase("farou9_abd_errehmane_hadraoui")){
                        athan = "farou9_abd_errehmane_hadraoui.mp3";
                    }else{
                        if(athanName.equalsIgnoreCase("mohammad_ali_el_banna")){
                            athan = "mohammad_ali_el_banna.mp3";
                        }else{
                            if(athanName.equalsIgnoreCase("mohammad_khalil_raml")){
                                athan = "mohammad_khalil_raml.mp3";
                            }
                        }
                    }
                }
            }

            AssetFileDescriptor descriptor = getAssets().openFd(athan);
            mediaPlayer.setDataSource(descriptor.getFileDescriptor(), descriptor.getStartOffset(), descriptor.getLength());
            descriptor.close();

            mediaPlayer.prepare();
            mediaPlayer.setLooping(false);
            mediaPlayer.start();
        } catch (Exception e) {
        }*/
    }

    private void onTimerFinish() {


        getNextPrayer();
        changeBackground();
        Date tomorrow = new Date();
        SimpleDateFormat newDateFormat = new SimpleDateFormat("HH:mm", Locale.US);
        currentTimeMinute = getMinutes(newDateFormat.format(tomorrow));
       // System.out.println(dateFormatTF.format(now));
        nextPrayerTimeInMinutes = nextPrayerTimeInMinutes - currentTimeMinute;
        nextPrayerinMilSeconds = TimeUnit.MINUTES.toMillis(nextPrayerTimeInMinutes);


        startTimer();
        setPrayerName();
        playAhan();

    }

    public static boolean isAfterDay(Calendar cal1, Calendar cal2) {
        if (cal1.get(Calendar.ERA) < cal2.get(Calendar.ERA)) return false;
        if (cal1.get(Calendar.ERA) > cal2.get(Calendar.ERA)) return true;
        if (cal1.get(Calendar.YEAR) < cal2.get(Calendar.YEAR)) return false;
        if (cal1.get(Calendar.YEAR) > cal2.get(Calendar.YEAR)) return true;
        return cal1.get(Calendar.DAY_OF_YEAR) > cal2.get(Calendar.DAY_OF_YEAR);
    }




    public int getMinutes(String time){
        String[] units = time.split(":"); //will break the string up into an array
        int hours = Integer.parseInt(units[0]); //first element
        int minutes = Integer.parseInt(units[1]); //second element
        return 60 * hours + minutes;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.times, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
      /*  if (id == R.id.action_map) {
            openPreferredLocationInMap();
            return true;
        }
        if (id == R.id.find_direction) {
            startActivity(new Intent(this, QiblaActivity.class));
            return true;
        }
        if (id == R.id.quran) {
            startActivity(new Intent(this, QuranActivity.class));
            return true;
        }
        if (id == R.id.dua_activity) {
            startActivity(new Intent(this, DuaActivity.class));
            return true;
        }

        if (id == R.id.about_activity) {
            startActivity(new Intent(this, AboutActivity.class));
            return true;
        }
        if (id == R.id.names_activity) {
            startActivity(new Intent(this, NamesActivity.class));
            return true;
        }*/

        return super.onOptionsItemSelected(item);
    }

    public void openPreferredLocationInMap() {

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(this, getString(R.string.need_location_permission_message), Toast.LENGTH_LONG).show();
            return;
        }
        try {
            // Start a new Activity for the Place Picker API, this will trigger {@code #onActivityResult}
            // when a place is selected or with the user cancels.
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            Intent i = builder.build(this);

            startActivityForResult(i, PLACE_PICKER_REQUEST);

        } catch (GooglePlayServicesRepairableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (GooglePlayServicesNotAvailableException e) {
            Log.e(TAG, String.format("GooglePlayServices Not Available [%s]", e.getMessage()));
        } catch (Exception e) {
            Log.e(TAG, String.format("PlacePicker Exception: %s", e.getMessage()));
        }
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_PICKER_REQUEST && resultCode == RESULT_OK) {
            Place place = PlacePicker.getPlace(this, data);


            if (place == null) {
                Log.i(TAG, "No place selected");
                return;
            }

            // Extract the place information from the API
            String placeName = place.getName().toString();
            String placeAddress = place.getAddress().toString();

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            editor.putString(getString(R.string.pref_location_key),placeAddress);
            editor.apply();

          TimesPreferences.setLocationDetails(this, place.getLatLng().latitude, place.getLatLng().longitude);

            latitude = place.getLatLng().latitude;
            longitude = place.getLatLng().longitude;

         refreshUI(latitude, longitude);
        }
    }

    private void refreshUI(double latitude, double longitude) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        city.setText(sharedPreferences.getString(getString(R.string.pref_location_key),
                getString(R.string.pref_location_default)));
        Log.d(TAG + "LocationLabel", sharedPreferences.getString(getString(R.string.pref_location_key), getString(R.string.pref_location_default)));

        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat mdformat = new SimpleDateFormat("dd-MMMM-yyyy");
        SimpleDateFormat mdformatYear = new SimpleDateFormat("yyyy");
        SimpleDateFormat mdformatMonth = new SimpleDateFormat("MM");
        SimpleDateFormat mdformatDay = new SimpleDateFormat("dd");

        String strDate = mdformat.format(calendar.getTime());
        String strDateHYear = mdformatYear.format(calendar.getTime());
        String strDateHMonth = mdformatMonth.format(calendar.getTime());
        String strDateHDay = mdformatDay.format(calendar.getTime());
        date_geo.setText(strDate);


        try {
            date_hijr.setText(new HijriTime(Calendar.getInstance(),getApplicationContext()).getHijriTime());
            date_geo.setText(new MiladiTime(Calendar.getInstance(), getApplicationContext()).getMiladiTime());
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Get NY time zone instance
        TimeZone defaultTz = TimeZone.getDefault();

        //Get NY calendar object with current date/time
        Calendar defaultCalc = Calendar.getInstance(defaultTz);

        //Get offset from UTC, accounting for DST
        int defaultTzOffsetMs = defaultCalc.get(Calendar.ZONE_OFFSET) + defaultCalc.get(Calendar.DST_OFFSET);
        final double timezone = defaultTzOffsetMs / (1000 * 60 * 60);



    /*    place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openPreferredLocationInMap();
            }
        });*/
        place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                releaseMediaPlayer();

                int result = mAudioManager.requestAudioFocus(mOnAudioFocusChangeListener,
                        AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN_TRANSIENT);

                if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
                    // We have audio focus now.
                    // Create and setup the {@link MediaPlayer} for the audio resource associated
                    // with the current word
                    //Log.d("AudioSourceInsideFromDB", Integer.toString(data.getInt(7)));
                    // Log.d("AudioSourceInside", Integer.toString(R.raw.dua_first));
                    player = MediaPlayer.create(MainActivity.this, R.raw.farou9_abd_errehmane_hadraoui);

                    // Start the audio file
                    player.start();

                    // Setup a listener on the media player, so that we can stop and release the
                    // media player once the sound has finished playing.
                    player.setOnCompletionListener(mCompletionListener);
                }

            }
        });

        // Test Prayer times here
        PrayTimes prayers = new PrayTimes();

        prayers.setTimeFormat(Integer.parseInt(sharedPreferences.getString(getString(R.string.pref_time_key),
                getString(R.string.pref_time_format_24_value))));
        prayers.setCalcMethod(Integer.parseInt(sharedPreferences.getString(getString(R.string.pref_method_key),
                getString(R.string.pref_method_mwl_value) )));
        prayers.setAsrJuristic(Integer.parseInt(sharedPreferences.getString(getString(R.string.pref_juristic_key),
                getString(R.string.pref_juristic_shafii_value) )));
        Log.d(TAG + "Preferences", sharedPreferences.getString(getString(R.string.pref_juristic_key),
                getString(R.string.pref_juristic_shafii_value) ));
        Log.d(TAG+"getCalcMethod", Integer.toString(prayers.getCalcMethod()));
        //method.setText(sharedPreferences.getString(getString(R.string.pref_method_key), getString(R.string.pref_method_label)));
        String method_number = sharedPreferences.getString(getString(R.string.pref_method_key), getString(R.string.pref_method_mwl_value));

        String method_label;
        switch (method_number){
            case "0":method_label = getString(R.string.pref_method_label_jafari);
                break;
            case "1":method_label = getString(R.string.pref_method_label_karachi);
                break;
            case "2":method_label = getString(R.string.pref_method_label_isna);
                break;
            case "3":method_label = getString(R.string.pref_method_label_mwl);
                break;
            case "4":method_label = getString(R.string.pref_method_label_qura);
                break;
            case "5":method_label = getString(R.string.pref_method_label_egypt);
                break;
            case "6":method_label = getString(R.string.pref_method_label_tehran);
                break;
            default:
                throw new IllegalArgumentException("Invalid day of the week: " + method_number);
        }
        method_label_textView.setText(method_label);

        prayers.setAdjustHighLats(prayers.AngleBased);
        int[] offsets = {0, 0, 0, 0, 0, 0, 0}; // {Fajr,Sunrise,Dhuhr,Asr,Sunset,Maghrib,Isha}
        prayers.tune(offsets);

        Date now = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(now);
        System.out.println(cal.getTime());
        ArrayList<String> prayerTimes = prayers.getPrayerTimes(cal,latitude, longitude, timezone);
        double [] testTimes =  prayers.getPrayerTimesDouble(cal, latitude, longitude, timezone);

        fajr.setText(prayerTimes.get(FAJR));
        sunrise.setText(prayerTimes.get(SUNRISE));
        zuhr.setText(prayerTimes.get(ZUHR));
        asr.setText(prayerTimes.get(ASR));
        magreeb.setText(prayerTimes.get(MAGREEB));
        isha.setText(prayerTimes.get(ISHA));

        for(int j=0;j<prayerTimes.size(); j++) {
            Log.d(TAG, prayerTimes.get(j));
        }

        for (int k=0; k<testTimes.length; k++) {
            Log.d(TAG+"Double", Double.toString(testTimes[k]));
        }

        Log.d (TAG+"Double", Double.toString(prayers.timeDiff(testTimes[0], testTimes[1])));

      /* Log.d(TAG+"MinutesGet", Integer.toString(getMinutes(testTimes[0])));
       Log.d(TAG+"MinutesGet", Integer.toString(getMinutes(testTimes[1])));
       Log.d(TAG+"MinutesGet", Integer.toString(getMinutes(testTimes[2])));
       Log.d(TAG+"MinutesGet", Integer.toString(getMinutes(testTimes[3])));
       Log.d(TAG+"MinutesGet", Integer.toString(getMinutes(testTimes[4])));
       Log.d(TAG+"MinutesGet", Integer.toString(getMinutes(testTimes[6])));*/

        Date today = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm", Locale.US);
        currentTimeMinute = getMinutes(dateFormat.format(today));
        allPrayrTimesInMinutes = new int[]{getMinutes(prayerTimes.get(FAJR)),getMinutes(prayerTimes.get(SUNRISE)),getMinutes(prayerTimes.get(ZUHR)),getMinutes(prayerTimes.get(ASR)),
                getMinutes(prayerTimes.get(MAGREEB)),getMinutes(prayerTimes.get(ISHA))};

        prayers.setAllPrayrTimesInMinutes(allPrayrTimesInMinutes);
        Log.d(TAG+"MinutesGet", Integer.toString(allPrayrTimesInMinutes[0]));
        Log.d(TAG, Integer.toString(allPrayrTimesInMinutes[1]));
        Log.d(TAG, Integer.toString(allPrayrTimesInMinutes[2]));
        Log.d(TAG, Integer.toString(allPrayrTimesInMinutes[3]));
        Log.d(TAG, Integer.toString(allPrayrTimesInMinutes[4]));
        Log.d(TAG, Integer.toString(allPrayrTimesInMinutes[5]));
        Log.d(TAG, Integer.toString(currentTimeMinute));




        getNextPrayer();
        changeBackground();
        //refreshTime();

        Log.d(TAG + "getPrayerTimeReturnMain", Integer.toString(actualPrayerCode));
        Log.d(TAG + "getPrayerTimeReturnMain", Integer.toString(nextPrayerTimeInMinutes));
        Log.d(TAG + "getPrayerTimeReturnMain", Integer.toString(currentTimeMinute));




            startTimer();


        sharedPreferences.registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(getString(R.string.pref_location_key))) {

            Log.d(TAG + "LocationLabel", sharedPreferences.getString(getString(R.string.pref_location_key), getString(R.string.pref_location_default)));

            double coord[] = TimesPreferences.getLocationCoordinates(this);

            latitude = coord[0];
            longitude = coord[1];
            countDownTimer.cancel();
            refreshUI(latitude, longitude);
        }
        else if  (key.equals(getString(R.string.pref_method_key))) {
            countDownTimer.cancel();
            refreshUI(latitude, longitude);
        }else if(key.equals(getString(R.string.pref_juristic_key))){
            countDownTimer.cancel();
            refreshUI(latitude, longitude);

        }
    }
    @Override
    public void onResume() {
        super.onResume();
        PreferenceManager.getDefaultSharedPreferences(this)
                .registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Unregister VisualizerActivity as an OnPreferenceChangedListener to avoid any memory leaks.
        PreferenceManager.getDefaultSharedPreferences(this)
                .unregisterOnSharedPreferenceChangeListener(this);
    }


    public static void  getNextPrayer() {//get time code name and his time in minutes
        Calendar calendar = Calendar.getInstance();
        int totalMinutes = (calendar.get(Calendar.HOUR_OF_DAY) * 60) + (calendar.get(Calendar.MINUTE));
        if (totalMinutes == 0 || totalMinutes == 1440 || (totalMinutes >= 0 && totalMinutes <= allPrayrTimesInMinutes[0])) {//if actual time is between 0 and fajr time , means that the next prayer time is fajr
            actualPrayerCode = 1025;//ishaa time code
            nextPrayerTimeInMinutes = allPrayrTimesInMinutes[0];
        } else {
            if (totalMinutes > allPrayrTimesInMinutes[0] && totalMinutes <= allPrayrTimesInMinutes[1]) {//if actual time is between fajr time and shorou9 time , means that the next prayer time is shorou9
                actualPrayerCode = 1020;//fajr time code
                nextPrayerTimeInMinutes = allPrayrTimesInMinutes[1];
            } else {
                if (totalMinutes > allPrayrTimesInMinutes[1] && totalMinutes <= allPrayrTimesInMinutes[2]) {//if actual time is between shorou9 time and duhr time , means that the next prayer time is duhr
                    actualPrayerCode = 1021;//shorou9 time code
                    nextPrayerTimeInMinutes = allPrayrTimesInMinutes[2];
                } else {
                    if (totalMinutes > allPrayrTimesInMinutes[2] && totalMinutes <= allPrayrTimesInMinutes[3]) {//if actual time is between duhr and asr time , means that the next prayer time is asr
                        actualPrayerCode = 1022;//duhr time code
                        nextPrayerTimeInMinutes = allPrayrTimesInMinutes[3];
                    } else {
                        if (totalMinutes > allPrayrTimesInMinutes[3] && totalMinutes <= allPrayrTimesInMinutes[4]) {//if actual time is between asr and maghrib time , means that the next prayer time is maghrib
                            actualPrayerCode = 1023;//asr time code
                            nextPrayerTimeInMinutes = allPrayrTimesInMinutes[4];
                        } else {
                            if (totalMinutes > allPrayrTimesInMinutes[4] && totalMinutes <= allPrayrTimesInMinutes[5]) {//if actual time is between maghrib and ishaa time , means that the next prayer time is ishaa
                                actualPrayerCode = 1024;//maghrib time code
                                nextPrayerTimeInMinutes = allPrayrTimesInMinutes[5];
                            } else {
                                if (totalMinutes > allPrayrTimesInMinutes[5] && totalMinutes < 1440) {//if actual time is between ishaa and 24H  , means that the next prayer time is fajr
                                    actualPrayerCode = 1025;//ishaa time code
                                    nextPrayerTimeInMinutes = allPrayrTimesInMinutes[0] + 1440;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void releaseMediaPlayer() {
        // If the media player is not null, then it may be currently playing a sound.
        if (player != null) {
            // Regardless of the current state of the media player, release its resources
            // because we no longer need it.
            player.reset();
            player.release();
            // Set the media player back to null. For our code, we've decided that
            // setting the media player to null is an easy way to tell that the media player
            // is not configured to play an audio file at the moment.
            player = null;
            // Regardless of whether or not we were granted audio focus, abandon it. This also
            // unregisters the AudioFocusChangeListener so we don't get anymore callbacks.
            mAudioManager.abandonAudioFocus(mOnAudioFocusChangeListener);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        releaseMediaPlayer();
    }
}
